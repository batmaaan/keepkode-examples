import com.fasterxml.jackson.databind.ObjectMapper;
import model.Country;
import model.Number;
import model.response.CountryResponse;
import model.response.NumberResponse;
import model.utils.UrlEndpoints;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ExampleOne {
  public static void main(String[] args) throws URISyntaxException, IOException, InterruptedException {
    Map<Country, List<String>> finalMap = new HashMap<>();
    HttpClient httpClient = HttpClient.newBuilder().build();
    ObjectMapper mapper = new ObjectMapper();

    HttpRequest countryRequest = HttpRequest.newBuilder()
      .uri(new URI(UrlEndpoints.FREE_COUNTRY_LIST_URL))
      .GET()
      .build();
    var countryResponce = httpClient.send(countryRequest, HttpResponse.BodyHandlers.ofString());

    var countryListResponse = mapper.readValue(countryResponce.body(), CountryResponse.class);

    var countryList = countryListResponse.getCountries();

    for (Country country : countryList) {
      HttpRequest numberRequest = HttpRequest.newBuilder()
        .uri(new URI(UrlEndpoints.PHONE_BY_COUNTRY_URL + country.getCountry()))
        .GET()
        .build();

      var numberResponce = httpClient.send(numberRequest, HttpResponse.BodyHandlers.ofString());

      var numberListResponse = mapper.readValue(numberResponce.body(), NumberResponse.class);
      var numberList = numberListResponse.getNumbers().stream().map(Number::getNumber).toList();
      finalMap.put(country, numberList);
    }
    System.out.println(finalMap);
  }
}
