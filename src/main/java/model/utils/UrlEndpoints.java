package model.utils;

public class UrlEndpoints {

  public static final String FREE_COUNTRY_LIST_URL = "https://onlinesim.ru/api/getFreeCountryList";
  public static final String PHONE_BY_COUNTRY_URL = "https://onlinesim.ru/api/getFreePhoneList?country=";
  public static final String PRICE_LIST = "https://onlinesim.ru/price-list";
}
