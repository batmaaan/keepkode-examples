package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Number {
  private String number;
  private int country;
  private String updated_at;
  private String data_humans;
  private String full_number;
  private String country_text;
  private String maxdate;
  private String status;
}
