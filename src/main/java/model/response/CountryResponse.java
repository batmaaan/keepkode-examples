package model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import model.Country;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CountryResponse {
  private String response;
  private List<Country> countries;
}
