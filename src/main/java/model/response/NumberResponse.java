package model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import model.Number;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NumberResponse {
  private String response;
  private List<Number> numbers;
}
