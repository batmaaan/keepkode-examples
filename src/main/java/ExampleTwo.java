import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.Map;

import static model.utils.UrlEndpoints.PRICE_LIST;

public class ExampleTwo {
  public static void main(String[] args) {
    Map<String, Map<String, String>> map;

    try {
      var page = Jsoup.connect(PRICE_LIST).get();
      var el = page.getElementsByClass("col-md-2 col-xs-6 country-block no-padding");
      for (Element element : el) {
        System.out.println();
      }
      System.out.println(el);

    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
